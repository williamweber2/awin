Affiliate Window Candidate Task
===============================


### Candidate


William Weber Silva


### Prerequisites

PHP 5.6+
Composer
Mysql Server

### Installing

Alter the config.php file to the credentials of your mysql database;

Install and run the composer to install the dependencies;

Run the installer.sql to create the table and insert the dummy data.

mysql -uYOUR-MYSQL-USER -pYOUR-MYSQL-PASSWORD YOUR-MYSQL_DATABASE < PATH-TO-PROJECT/database/installer.sql

### Execute

php report.php 1

### Test

vendor/bin/phpunit -c test/phpunit.xml



