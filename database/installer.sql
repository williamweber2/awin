CREATE DATABASE IF NOT EXISTS awin;

DROP TABLE IF EXISTS transaction;

CREATE TABLE `transaction` (
  `merchant_id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `currency_symbol` char(1) DEFAULT '£',
  `amount` int(11) DEFAULT 0,
  `currency_code` varchar(3) DEFAULT 'GBP'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `transaction` VALUES
(1,'2017-06-20 00:00:00','£',20000, 'GBP'),
(1,'2017-06-21 00:00:00','£',30000, 'GBP'),
(1,'2017-06-22 00:00:00','$',40000, 'USD'),
(1,'2017-06-22 00:00:00','€',50000, 'EUR');
