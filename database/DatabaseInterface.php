<?php

namespace Awin\Database;

/**
 * The interface for database connections. The estipulated contract guarantees that these public methods
 * will be mandatory to the classes which implements this interface.
 *
 * Interface DatabaseInterface
 * @package Awin\Database
 */
interface DatabaseInterface
{
    /**
     * DatabaseInterface constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection);

    /**
     * @param $sqlStatement
     * @return mixed
     */
    public function query($sqlStatement);

    /**
     * @param $result
     * @return mixed
     */
    public function getAssoc($result);

    /**
     * @return mixed
     */
    public function close();
}
