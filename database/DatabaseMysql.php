<?php

namespace Awin\Database;

/**
 * This class controls how the construct, query and getAssoc methods should
 * work when dealing with a mysql database
 *
 * Class DatabaseMysql
 * @package Awin\Database
 */
class DatabaseMysql implements DatabaseInterface
{
    private $connection;

    /**
     * DatabaseMysql constructor.
     * @param Connection $connection
     * @throws \Exception
     */
    public function __construct(Connection $connection)
    {
        try {
            $this->connection = mysqli_connect(
                $connection->getHost(),
                $connection->getUsername(),
                $connection->getPassword(),
                $connection->getDatabase(),
                $connection->getPort()
            );
            $this->connection->set_charset('utf8');

        } catch (\Exception $e) {
            throw new \Exception("Erro na conexão com MySQL", $e);
        }

        return $this->connection;
    }

    /**
     * @param $sqlStatement
     * @return mixed
     */
    public function query($sqlStatement)
    {
        try {
            $mysqlResult = mysqli_query($this->connection, $sqlStatement);
            if (!$mysqlResult) {
                throw new \Exception(mysqli_error($this->connection));
            } else {
                return $mysqlResult;
            }
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }

    }

    /**
     * @param $result
     * @return array
     */
    public function getAssoc($result)
    {
        try {
            $rows = array();
            if ($result) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $rows[] = $row;
                }
            }
            return $rows;
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
    }


    public function close()
    {
        $this->connection->close();
    }
}
