<?php

namespace Awin\Database;

class DatabaseFactory
{
    const MYSQL_CONNECTION = 'mysql';
    private $mysqlConnection;

    /**
     * The getConnection method returns an object that implements Database Interface.
     * The default behaviour is to return a connection to Mysql Database
     * 
     * 
     * @param string $connectionName
     * @return DatabaseMysql|null
     */
    public function getConnection($connectionName = self::MYSQL_CONNECTION)
    {
        $connection = null;
        switch ($connectionName) {
            case self::MYSQL_CONNECTION:
                $connection = $this->getMysqlConnection();
                break;
        }

        return $connection;
    }

    /**
     * The method to return a mysql connection, you can implement another
     * method to return a SqlServer or MongoDb connection for example. This
     * one is for mysql connections.
     * 
     * @return DatabaseMysql
     */
    private function getMysqlConnection()
    {
        if (empty($this->mysqlConnection)) {
            $connectionObject = new Connection();
            $connectionObject
                ->setHost(MYSQL_HOST)
                ->setDatabase(MYSQL_DATABASE)
                ->setUsername(MYSQL_USERNAME)
                ->setPassword(MYSQL_PASSWORD)
                ->setPort(MYSQL_PORT);

            $this->mysqlConnection = new DatabaseMysql($connectionObject);
        }

        return $this->mysqlConnection;
    }
}
