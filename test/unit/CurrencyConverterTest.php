<?php

namespace Awin\Test\Unit;

use Awin\Entity\DollarEntity;
use Awin\Entity\EuroEntity;
use Awin\Entity\PoundEntity;
use Awin\Model\Currency\CurrencyConverter;
use Awin\Model\Currency\CurrencyTable;
use PHPUnit\Framework\TestCase;

/**
 * Unit tests for Currency Converter Class.
 *
 * Class CurrencyConverterTest
 * @package Awin\Test\Unit
 */
class CurrencyConverterTest extends TestCase
{

    /**
     * Tests the method convert of dollar currency to pound currency.
     * Since the method is very simple, I didn't see the need
     * of mocking the entities.
     */
    public function testFromDollarToPound()
    {
        $sampleAmount = $this->getSampleAmount();
        $currencyTable = new CurrencyTable();
        $currencyConverter = new CurrencyConverter($currencyTable);

        $fromCurrency = new DollarEntity();
        $fromCurrency->setAmount($sampleAmount);
        $toCurrency = new PoundEntity();

        $currencyConverter->convert($fromCurrency, $toCurrency);
        $expectedResult = CurrencyTable::table()[DollarEntity::CURRENCY_CODE][PoundEntity::CURRENCY_CODE]*$sampleAmount;
        $this->assertEquals($toCurrency->getAmount(), $expectedResult, "Conversion from Dollar to Pound did match the expected result.");
    }

    /**
     * Tests the method convert of euro currency to pound currency.
     *
     */
    public function testFromEuroToPound()
    {
        $sampleAmount = $this->getSampleAmount();
        $currencyTable = new CurrencyTable();
        $currencyConverter = new CurrencyConverter($currencyTable);

        $fromCurrency = new EuroEntity();
        $fromCurrency->setAmount($sampleAmount);
        $toCurrency = new PoundEntity();

        $currencyConverter->convert($fromCurrency, $toCurrency);
        $expectedResult = CurrencyTable::table()[EuroEntity::CURRENCY_CODE][PoundEntity::CURRENCY_CODE]*$sampleAmount;
        $this->assertEquals($toCurrency->getAmount(), $expectedResult, "Conversion from EURO to Pound did match the expected result.");
    }

    /**
     * Returns a random value.
     * @return mixed
     */
    public function getSampleAmount()
    {
        return mt_rand(1, 999);
    }
}