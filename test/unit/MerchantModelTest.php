<?php

namespace Awin\Test\Unit;

use Awin\Database\Connection;
use Awin\Database\DatabaseFactory;
use Awin\Database\DatabaseMysql;
use Awin\Entity\DollarEntity;
use Awin\Entity\EntityFactory;
use Awin\Entity\PoundEntity;
use Awin\Entity\TransactionEntity;
use Awin\Model\Currency\CurrencyConverter;
use Awin\Model\Currency\CurrencyTable;
use Awin\Model\Merchant\MerchantModel;
use PHPUnit\Framework\TestCase;

/**
 * Unit Tests to the Merchant Model Class.
 *
 * Class MerchantModelTest
 * @package Awin\Test\Unit
 */
class MerchantModelTest extends TestCase
{
    private $databaseFactoryMock;
    private $entityFactoryMock;
    private $currencyConverterMock;
    private $databaseMysqlMock;
    private $currencyTableMock;
    private $transactionEntityMock;

    /**
     * Unit test to the method getTransactionsByMerchantId.
     * All dependencies were mocked in order to isolate the unit and test it.
     */
    public function testTransactionByMerchantId()
    {
        $merchantId = 1;
        $queryTransactions = "SELECT * FROM transaction where merchant_id = {$merchantId}";
        $this->buildMocks();
        $this->transactionEntityMock->expects($this->exactly(2))->method("fromArray")
            ->withConsecutive(
                [$this->getTransactionsMockData()[0]],
                [$this->getTransactionsMockData()[1]]
            )->will($this->onConsecutiveCalls(
                $this->getEntityTransactionsMockData()[0],
                $this->getEntityTransactionsMockData()[1]
                )
            );
        $this->entityFactoryMock->expects($this->exactly(2))->method("getEntity")->willReturn($this->transactionEntityMock);
        $this->databaseMysqlMock->expects($this->once())->method("query")->with($queryTransactions)->willReturn("mysql_result");
        $this->databaseMysqlMock->expects($this->once())->method("getAssoc")->with("mysql_result")->willReturn($this->getTransactionsMockData());
        $this->databaseFactoryMock->expects($this->once())->method("getConnection")->willReturn($this->databaseMysqlMock);
        $merchantModel = new MerchantModel($this->entityFactoryMock, $this->databaseFactoryMock, $this->currencyConverterMock);
        $merchantModel->getTransactionsByMerchantId($merchantId);
    }

    /**
     * Unit test to the method convertTransactionsToCurrency.
     * All dependencies were mocked in order to isolate the unit and test it.
     */
    public function testConvertTransactionsToCurrency()
    {
        $this->buildMocks();

        $dollarEntityMock = $this->getMockBuilder(DollarEntity::class)->setMethods(["setAmount"])->getMock();
        $dollarEntityMock->expects($this->once())->method("setAmount")->with($this->getEntityTransactionsMockData()[1]->amount);

        $poundEntityMock = $this->getMockBuilder(PoundEntity::class)->setMethods(["getAmount"])->getMock();
        $poundEntityMock->expects($this->once())->method("getAmount");

        $this->entityFactoryMock->expects($this->exactly(2))->method("getEntity")
            ->will($this->onConsecutiveCalls(
                $dollarEntityMock,
                $poundEntityMock
            ));

        $this->currencyConverterMock->expects($this->once())->method("convert")->with($dollarEntityMock, $poundEntityMock);
        $merchantModel = new MerchantModel($this->entityFactoryMock, $this->databaseFactoryMock, $this->currencyConverterMock);
        $merchantModel->convertTransactionsToCurrency($this->getEntityTransactionsMockData(), "GBP");
    }

    /**
     * This creates and injects de dependencies as mocks.
     */
    private function buildMocks()
    {
        $this->currencyTableMock = $this->getMockBuilder(CurrencyTable::class)->setMethods(["getTableByCurrency"])->getMock();
        $this->databaseFactoryMock = $this->getMockBuilder(DatabaseFactory::class)->setMethods(["getConnection"])->getMock();
        $this->entityFactoryMock = $this->getMockBuilder(EntityFactory::class)->setMethods(["getEntity"])->getMock();
        $this->currencyConverterMock = $this->getMockBuilder(CurrencyConverter::class)->setConstructorArgs([$this->currencyTableMock])->getMock();
        $this->databaseMysqlMock = $this->getMockBuilder(DatabaseMysql::class)->disableOriginalConstructor()->setMethods(["query", "getAssoc"])->getMock();
        $this->transactionEntityMock = $this->getMockBuilder(TransactionEntity::class)->setMethods(["fromArray", "toArray"])->getMock();

    }

    /**
     * Returns a sample value of a database return.
     * @return array
     */
    private function getTransactionsMockData()
    {
        return [
          ["merchant_id" => 1, "date" => "2017-01-01", "currency_symbol" => "£", "amount" => "100", "currency_code"=>"GBP"],
          ["merchant_id" => 2, "date" => "2017-01-02", "currency_symbol" => "$", "amount" => "200", "currency_code" => "USD"]
        ];
    }

    /**
     * Returns a sample value of an array of Transaction Entities.
     * @return array
     */
    private function getEntityTransactionsMockData()
    {
        $transaction1 = new TransactionEntity();
        $transaction1->merchantId = 1;
        $transaction1->date = "2017-01-01";
        $transaction1->amount = "100";
        $transaction1->currencySymbol = "£";
        $transaction1->currencyCode = "GBP";

        $transaction2 = new TransactionEntity();
        $transaction2->merchantId = 2;
        $transaction2->date = "2017-01-02";
        $transaction2->amount = "200";
        $transaction2->currencySymbol = "$";
        $transaction2->currencyCode = "USD";

        return [
            $transaction1,
            $transaction2
        ];
    }
}