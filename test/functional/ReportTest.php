<?php

namespace Awin\Test\Functional;

class ReportTest extends \PHPUnit\Framework\TestCase
{

    /**
     * Functional test to verify if the program, works as expected.
     */
    public function testFromDollarToPound()
    {
        $funcionalResult = `php report.php 1`;
        $this->assertEquals($this->getReportResult(), $funcionalResult);
    }

    /**
     * Get expected result.
     *
     * @return string
     */
    private function getReportResult()
    {
        return "merchant_id              	date                     	currency_symbol          	currency_code            	amount                   \n1                        	2017-06-20 00:00:00      	£                       	GBP                      	200.00                   \n1                        	2017-06-21 00:00:00      	£                       	GBP                      	300.00                   \n1                        	2017-06-22 00:00:00      	$                        	USD                      	400.00                   \n1                        	2017-06-22 00:00:00      	€                      	EUR                      	500.00                   \nmerchant_id              	date                     	currency_symbol          	currency_code            	amount                   \n1                        	2017-06-20 00:00:00      	£                       	GBP                      	200.00                   \n1                        	2017-06-21 00:00:00      	£                       	GBP                      	300.00                   \n1                        	2017-06-22 00:00:00      	£                       	GBP                      	100.00                   \n1                        	2017-06-22 00:00:00      	£                       	GBP                      	250.00                   \n";
    }
}