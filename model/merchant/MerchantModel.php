<?php

namespace Awin\Model\Merchant;

use Awin\Database\DatabaseFactory;
use Awin\Entity\DollarEntity;
use Awin\Entity\EntityFactory;
use Awin\Entity\EuroEntity;
use Awin\Model\AbstractModel;
use Awin\Model\Currency\CurrencyConverter;

/**
 * The Merchant Model is responsible to manage the Merchant Information.
 * Despite the fact that the Transacion should has its own model, I didn't think
 * this would be necessary in this task.
 *
 * This class returns the transactions of a given merchant id, convert all
 * transaction amounts to a given currency and renders the result table to be
 * displayed in the terminal.
 *
 * Class MerchantModel
 * @package Awin\Model\Merchant
 */
class MerchantModel extends AbstractModel
{
    protected $currencyConverter;

    /**
     * Here I inject a entity factory, a database factory and the currency converter.
     *
     * This construct method overrides the AbstractModel because
     * I don't think that other models of this system should use the
     * currency converter, this appears to be particularity of this
     * class.
     *
     * MerchantModel constructor.
     * @param EntityFactory $entityFactory
     * @param DatabaseFactory $databaseFactory
     * @param CurrencyConverter $currencyConverter
     */
    public function __construct(EntityFactory $entityFactory, DatabaseFactory $databaseFactory, CurrencyConverter $currencyConverter)
    {
        $this->entityFactory = $entityFactory;
        $this->databaseFactory = $databaseFactory;
        $this->currencyConverter = $currencyConverter;
        $this->conn = $this->databaseFactory->getConnection();
    }

    /**
     * Returns a array of Transaction Entities objects to the
     * returned transactions of a give merchant id.
     *
     * @param $merchantId
     * @return array
     */
    public function getTransactionsByMerchantId($merchantId)
    {
        $returnTransactions = [];
        $getTransactionsQuery = "SELECT * FROM transaction where merchant_id = {$merchantId}";
        $getTransactionsResult = $this->conn->query($getTransactionsQuery);
        $transactions  = $this->conn->getAssoc($getTransactionsResult);

        foreach ($transactions as $transaction) {
            $transactionEntity = $this->entityFactory->getEntity("TransactionEntity");
            $transactionEntity->fromArray($transaction);
            $returnTransactions[] = $transactionEntity;
        }
        return $returnTransactions;
    }

    /**
     * This method converts all transactions to a give currency.
     * It applies the conversion rates and change the currency Symbol
     * and Code.
     *
     * @param $transactions
     * @param $currencyCode
     */
    public function convertTransactionsToCurrency($transactions, $currencyCode)
    {
        foreach ($transactions as $transaction) {
            if ($transaction->currencyCode != $currencyCode) {
                $fromCurrencyEntity = $this->getCurrencyEntityFromCode($transaction->currencyCode);
                $fromCurrencyEntity->setAmount($transaction->amount);
                $toCurrencyEntity = $this->getCurrencyEntityFromCode($currencyCode);
                $this->currencyConverter->convert($fromCurrencyEntity, $toCurrencyEntity);
                $transaction->currencyCode = $currencyCode;
                $transaction->currencySymbol = $toCurrencyEntity->getCurrencySymbol();
                $transaction->amount = $toCurrencyEntity->getAmount();
            }
        }
    }

    /**
     * Renders a transactions table of a give array
     * of transaction entities.
     *
     * @param $transactions
     */
    public function renderTransactionTable($transactions)
    {
        if (!empty($transactions)) {
            $tableContent = "";

            $tableHeaderArray = array_keys(
                $transactions[0]->toArray()
            );

            array_walk($tableHeaderArray, [$this, "applyPaddingToElements"]);

            $tableHeader = implode("\t", $tableHeaderArray)."\n";


            foreach ($transactions as $transaction) {
                $transaction = $transaction->toArray();
                array_walk($transaction, [$this, "applyPaddingToElements"]);
                $tableContent .= implode("\t", $transaction)."\n";
            }

            echo $tableHeader.$tableContent;
        }

    }

    /**
     * Applies a padding just to improve the display. Nothing special.
     *
     * @param $element
     * @param $key
     * @return mixed
     */
    private function applyPaddingToElements(&$element, $key)
    {
        if ($key == "amount" && is_numeric($element)) {
            $element = number_format($element/100, 2);
        }
        $element = str_pad($element, 25);
        return $element;
    }

    /**
     * Returns a Currency Entity of a given Currency Code.
     * 
     * @param $currencyCode
     * @return object
     */
    private function getCurrencyEntityFromCode($currencyCode)
    {
        switch ($currencyCode) {
            case DollarEntity::CURRENCY_CODE:
                return $this->entityFactory->getEntity("DollarEntity");
                break;
            case EuroEntity::CURRENCY_CODE:
                return $this->entityFactory->getEntity("EuroEntity");
                break;
            default:
                return $this->entityFactory->getEntity("PoundEntity");
                break;
        }
    }

}