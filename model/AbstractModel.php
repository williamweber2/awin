<?php

namespace Awin\Model;

use Awin\Database\DatabaseFactory;
use Awin\Entity\EntityFactory;

/**
 * An abstract class created to provide the default constructor
 * to the models.
 *
 * Class AbstractModel
 * @package Awin\Model
 */
abstract class AbstractModel
{
    protected $entityFactory;
    protected $databaseFactory;
    protected $conn;

    public function __construct(EntityFactory $entityFactory, DatabaseFactory $databaseFactory)
    {
        $this->entityFactory = $entityFactory;
        $this->databaseFactory = $databaseFactory;
        $this->conn = $this->databaseFactory->getConnection();
    }
}