<?php

namespace Awin\Model\Currency;

use Awin\Entity\CurrencyEntityInterface;

/**
 * Well this is currency converter, it's responsibility is to convert
 * a currency pair, FROM and TO given Currency Entities.
 *
 * Class CurrencyConverter
 * @package Awin\Model\Currency
 */
class CurrencyConverter
{
    protected $currencyTable;

    /**
     * The currency table is injected in this class. This injected
     * object holds the conversion rates between currencies.
     *
     * CurrencyConverter constructor.
     * @param CurrencyTable $currencyTable
     */
    public function __construct(CurrencyTable $currencyTable)
    {
        $this->currencyTable = $currencyTable;
    }

    /**
     * The convert method, receives the currency pair FROM and TO, it applies the conversion
     * rate which is located in the currency table and then updates the amount of the TO currency
     * entity.
     *
     * @param CurrencyEntityInterface $currencyFrom
     * @param CurrencyEntityInterface $currencyTo
     */
    public function convert(CurrencyEntityInterface $currencyFrom, CurrencyEntityInterface $currencyTo)
    {
        $currencyTable = $this->currencyTable->getTableByCurrency($currencyFrom->getCurrencyCode());

        $currencyRate = $currencyTable[$currencyTo->getCurrencyCode()];

        $convertedAmount = $currencyFrom->getAmount() * $currencyRate;

        $currencyTo->setAmount($convertedAmount);

    }
}