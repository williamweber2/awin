<?php

namespace Awin\Model\Currency;

/**
 * This is a simple class which holds the conversion rates between currencies.
 *
 * Class CurrencyTable
 * @package Awin\Model\Currency
 */
class CurrencyTable
{
    /**
     * This returns the conversion rate to a given currency code.
     *
     * @param $currencyCode
     * @return mixed
     */
    public function getTableByCurrency($currencyCode)
    {
        $currencyTable = self::table();
        return $currencyTable[$currencyCode];
    }

    public static function table()
    {
        return [
            "GBP" => [
                "EUR" => 2,
                "USD" => 4
            ],
            "EUR" => [
                "GBP" => 0.5,
                "USD" => 2
            ],
            "USD" => [
                "GBP" => 0.25,
                "EUR" => 0.5
            ]
        ];
    }


}