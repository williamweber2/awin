<?php

namespace Awin\Entity;

/**
 * Pound Entity implements the CurrencyEntityInterface and inherit
 * the behaviours of AbstractCurrencyEntity.
 *
 * Class PoundEntity
 * @package Awin\Entity
 */
class PoundEntity extends AbstractCurrencyEntity implements CurrencyEntityInterface
{
    const CURRENCY_SYMBOL = "£";
    const CURRENCY_CODE = "GBP";
}