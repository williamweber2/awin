<?php

namespace Awin\Entity;

/**
 * Interface for the currency entities to guarantee that
 * all classes implement those methods. Every Currency entity
 * should manipulate the amount and return both currecy symbol
 * and code.
 *
 * Interface CurrencyEntityInterface
 * @package Awin\Entity
 */
interface CurrencyEntityInterface
{
    public function setAmount($amount);

    public function getAmount();

    public function getCurrencySymbol();

    public function getCurrencyCode();
}