<?php

namespace Awin\Entity;

class EntityFactory
{
    public function getEntity($entityClassName)
    {
        $entityNamespace = "Awin\\Entity\\";
        $reflectedClass = new \ReflectionClass($entityNamespace.$entityClassName);
        return $reflectedClass->newInstance();
    }
}