<?php

namespace Awin\Entity;

/**
 * This class was created to avoid repeated code. The abstract class sets common behaviours to be
 * used by children classes.
 *
 * Class AbstractCurrencyEntity
 * @package Awin\Entity
 */
abstract class AbstractCurrencyEntity
{
    const CURRENCY_SYMBOL = "£";
    const CURRENCY_CODE = "GBP";

    private $amount;

    /**
     * @param $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCurrencySymbol()
    {
        return static::CURRENCY_SYMBOL;
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
        return static::CURRENCY_CODE;
    }
}