<?php

namespace Awin\Entity;

/**
 * The transaction entity was created just to organize the
 * transaction data when passed through methods. We can
 * populate the class attributes with a given array and
 * vice versa.
 *
 * Class TransactionEntity
 * @package Awin\Entity
 */
class TransactionEntity
{
    public $merchantId;
    public $date;
    public $currencySymbol;
    public $amount;
    public $currencyCode;

    /**
     * @param $arrayData
     */
    public function fromArray($arrayData)
    {
        $this->merchantId = $arrayData["merchant_id"];
        $this->date = $arrayData["date"];
        $this->currencySymbol = $arrayData["currency_symbol"];
        $this->amount = $arrayData["amount"];
        $this->currencyCode = $arrayData["currency_code"];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $returnArray = [
            "merchant_id" => $this->merchantId,
            "date" => $this->date,
            "currency_symbol" => $this->currencySymbol,
            "currency_code" => $this->currencyCode,
            "amount" => $this->amount
        ];
        
        return $returnArray;
    }
}