<?php

namespace Awin\Entity;

/**
 * Euro Entity implements the CurrencyEntityInterface and inherit
 * the behaviours of AbstractCurrencyEntity.
 *
 * Class EuroEntity
 * @package Awin\Entity
 */
class EuroEntity extends AbstractCurrencyEntity implements CurrencyEntityInterface
{
    const CURRENCY_SYMBOL = "€";
    const CURRENCY_CODE = "EUR";
}