<?php

namespace Awin\Entity;

/**
 * Dollar Entity implements the CurrencyEntityInterface and inherit
 * the behaviours of AbstractCurrencyEntity.
 *
 * Class DollarEntity
 * @package Awin\Entity
 */
class DollarEntity extends AbstractCurrencyEntity implements CurrencyEntityInterface
{
    const CURRENCY_SYMBOL = "$";
    const CURRENCY_CODE = "USD";
}