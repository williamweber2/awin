<?php
/**
 * Here is where everything happens.
 * All dependencies are injected and the the methods are called.
 */
require ("vendor/autoload.php");
require ("config.php");

$databaseFactory = new \Awin\Database\DatabaseFactory();
$entityFactory = new \Awin\Entity\EntityFactory();
$currencyTable = new \Awin\Model\Currency\CurrencyTable();
$currencyConverter = new \Awin\Model\Currency\CurrencyConverter($currencyTable);
$merchantModel = new \Awin\Model\Merchant\MerchantModel($entityFactory, $databaseFactory, $currencyConverter);
($argv[1])?$merchantId = $argv[1]:$merchantId = 1;
$transactions = $merchantModel->getTransactionsByMerchantId($merchantId);
$merchantModel->renderTransactionTable($transactions);
$merchantModel->convertTransactionsToCurrency($transactions, \Awin\Entity\PoundEntity::CURRENCY_CODE);
$merchantModel->renderTransactionTable($transactions);



